package application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client_1 {
	

	
	public static void main(String[] args) {
		try {

			Scanner scan = new Scanner(System.in);
			String pseudo;			
			System.out.println ("\nEntrer votre pseudo : ");
			pseudo = scan.next();
			Socket socket_client = new Socket ("localhost", 10080);
			MessageReceptorClient msgrecepclient = new MessageReceptorClient(socket_client);
			msgrecepclient.start();
			MessageTransmitterClient msgtransclient = new MessageTransmitterClient(socket_client, pseudo, scan);
			msgtransclient.start();
		} catch (IOException ex) {
			Logger.getLogger(ServerSocket.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}